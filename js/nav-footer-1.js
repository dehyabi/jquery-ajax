$(document).ready(()=>{
     $("#nav").load("nav/nav-4.html", function(response, status, xhr){
       if (status == 'error') {
         let msg = 'Nav Error: ';
         $('#error').html(msg + xhr.status + " " + xhr.statusText);
       }
     });
     
     $('#footer').load('footer/footer-1.html', (response, status, xhr)=>{
      
        const d = new Date();
        let year = d.getFullYear();
        $('#footerContent').text(`This is footer ${year}`);
       
       if(status == 'error')
       $('#errorFooter').text(`Footer Error: ${xhr.status} ${xhr.statusText}`);
     });
     
     
     });